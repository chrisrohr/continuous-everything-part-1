package com.fortitudetec.resources;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.fortitudetec.core.User;
import com.fortitudetec.db.UserDAO;

import java.util.List;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

@Path("/users")
@Produces("application/json")
@Consumes("application/json")
public class UserResource {

    private UserDAO dao;

    public UserResource(UserDAO dao) {
        this.dao = dao;
    }

    @GET
    @Timed @ExceptionMetered
    public List<User> users() {
        return dao.findUsers();
    }

    @GET
    @Path("{id}")
    @Timed @ExceptionMetered
    public User user(@PathParam("id") int id) {
        return dao.findUserById(id);
    }

    @POST
    @Timed @ExceptionMetered
    public Response create(@Valid User user) {
        int id = dao.insert(user);
        return Response.created(UriBuilder.fromPath(Integer.toString(id)).build()).build();
    }

    @DELETE
    @Path("{id}")
    @Timed @ExceptionMetered
    public Response delete(@PathParam("id") int id) {
        dao.delete(id);
        return Response.noContent().build();
    }
}
