package com.fortitudetec.db;

import com.fortitudetec.core.User;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import java.util.List;

@RegisterMapper(UserMapper.class)
public interface UserDAO {
    @SqlQuery("select * from users;")
    List<User> findUsers();

    @SqlQuery("select * from users where id = :id")
    User findUserById(@Bind("id") int id);

    @GetGeneratedKeys
    @SqlUpdate("insert into users (name, email, phone_number) values (:name, :email, :phoneNumber)")
    int insert(@BindBean User user);

    @SqlUpdate("delete from users where id = :id")
    void delete(@Bind("id") int id);
}
