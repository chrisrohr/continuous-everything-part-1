package com.fortitudetec;

import com.fortitudetec.db.UserDAO;
import com.fortitudetec.resources.UserResource;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.jdbi.DBIHealthCheck;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

public class DemoApplication extends Application<DemoConfiguration> {

    public static void main(final String[] args) throws Exception {
        new DemoApplication().run(args);
    }

    @Override
    public String getName() {
        return "Continuous Everything Demo";
    }

    @Override
    public void initialize(final Bootstrap<DemoConfiguration> bootstrap) {

        bootstrap.addBundle(new MigrationsBundle<DemoConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(DemoConfiguration configuration) {
                return configuration.getDatabase();
            }
        });

        bootstrap.setConfigurationSourceProvider(
            new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                new EnvironmentVariableSubstitutor()
            )
        );
    }

    @Override
    public void run(final DemoConfiguration configuration, final Environment environment) {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDatabase(), "postgresql");
        final UserDAO dao = jdbi.onDemand(UserDAO.class);
        environment.jersey().register(new UserResource(dao));
        environment.healthChecks().register("DBI", new DBIHealthCheck(jdbi, configuration.getDatabase().getValidationQuery()));
    }

}
