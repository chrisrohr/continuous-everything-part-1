# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  # Sets up the mounted directory
  config.vm.synced_folder '.', '/vagrant'

  # Continuous Delivery VM (Puppet)
  config.vm.define 'cd-puppet' do |d|
    d.vm.box = 'ubuntu/trusty64'
    d.vm.hostname = 'cd'
    d.vm.network 'private_network', ip: '10.100.198.200'
    d.vm.provider 'virtualbox' do |v|
      v.memory = 2048
    end
    d.vm.provision :shell do |shell|
      shell.inline = "mkdir -p /etc/puppet/modules;
                      puppet module install garethr-docker;"
    end

    d.vm.provision :puppet do |puppet|
      puppet.manifests_path = 'puppet/manifests'
      puppet.module_path = 'puppet/modules'
      puppet.options = ['--verbose']
    end
  end

  # Continuous Delivery VM (Ansibile)
  config.vm.define 'cd-ansible' do |d|
    d.vm.box = 'ubuntu/trusty64'
    d.vm.hostname = 'cd'
    d.vm.network 'private_network', ip: '10.100.198.200'
    d.vm.provision :shell, path: 'ansible/scripts/bootstrap_ansible.sh'
    d.vm.provision :shell, inline: 'PYTHONUNBUFFERED=1 ansible-playbook /vagrant/ansible/cd.yml -c local'
    d.vm.provider 'virtualbox' do |v|
      v.memory = 2048
    end
  end

  # Deployment VM
  config.vm.define 'dev' do |d|
    d.vm.box = 'ubuntu/trusty64'
    d.vm.hostname = 'dev'
    d.vm.network 'private_network', ip: '10.100.198.201'
    d.vm.provider 'virtualbox' do |v|
      v.memory = 1024
    end
  end

  (1..3).each do |i|
    config.vm.define "serv-disc-0#{i}" do |d|
      d.vm.box = "ubuntu/trusty64"
      d.vm.hostname = "serv-disc-0#{i}"
      d.vm.network "private_network", ip: "10.100.194.20#{i}"
      d.vm.provider "virtualbox" do |v|
        v.memory = 1024
      end
    end
  end

  # Proxy Service VM
  config.vm.define 'proxy' do |d|
    d.vm.box = 'ubuntu/trusty64'
    d.vm.hostname = 'proxy'
    d.vm.network 'private_network', ip: '10.100.193.200'
    d.vm.provider 'virtualbox' do |v|
      v.memory = 1024
    end
  end

  # Caches images for faster startup
  config.cache.scope = :box if Vagrant.has_plugin?('vagrant-cachier')

  # Auto installs the guest additions
  if Vagrant.has_plugin?('vagrant-vbguest')
    config.vbguest.auto_update = false
    config.vbguest.no_install = true
    config.vbguest.no_remote = true
  end
end
