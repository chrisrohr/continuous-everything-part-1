# Sets up Docker on the VM
class docker_setup {
  include docker

  docker::run { 'dockerui':
    image   => 'uifd/ui-for-docker',
    ports   => '9000:9000',
    volumes => '/var/run/docker.sock:/var/run/docker.sock',
    require => Service['docker']
  }
}
