# Add VMs to host file
class common::host_entries {
  host { 'cd':
    ip => '10.100.198.200',
  }
  host { 'dev':
    ip => '10.100.198.201',
  }
  host { 'proxy':
    ip => '10.100.193.200',
  }
}
