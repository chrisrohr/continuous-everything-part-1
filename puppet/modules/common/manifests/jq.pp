# Ensure JQ is installed
class common::jq {
  package { 'jq':
    ensure => 'installed'
  }
}
